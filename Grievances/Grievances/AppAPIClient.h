//
// Created by Rajesh  on 6/9/15.
// Copyright (c) 2015 Rajesh. All rights reserved.
//


#import "AFHTTPSessionManager.h"
#import "AFNetworking+PromiseKit.h"

@interface AppAPIClient : AFHTTPSessionManager

+ (AppAPIClient *) sharedClient;
+ (NSMutableString *)endpoint:(NSString *)endpoint, ...;
+ (AFPromise *)GET:(NSString *)urlString parameters:(id)parameters;
+ (AFPromise *)HEAD:(NSString *)urlString parameters:(id)parameters;
+ (AFPromise *)POST:(NSString *)urlString parametersAsFormData:(id)parameters;
+ (AFPromise *)POST:(NSString *)urlString parameters:(id)parameters ;

@end
