//
//  ViewController.m
//  Grievances
//
//  Created by Rajesh on 6/22/16.
//  Copyright © 2016 Org. All rights reserved.
//

#import "AGInitialViewController.h"

@interface AGInitialViewController ()
@property (weak, nonatomic) IBOutlet UIButton *tamilButton;
@property (weak, nonatomic) IBOutlet UIButton *englishButton;
@property (weak, nonatomic) IBOutlet UILabel *chooseLangageLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@end

@implementation AGInitialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureHamburgerButton];
    [self configureView];
    [_englishButton setTitle:@"ENGLISH" forState:UIControlStateNormal];
    [_tamilButton setTitle:@"தமிழ்" forState:UIControlStateNormal];
}

- (void)configureView {
    if (self.isTamilLanguage) {
//        [_contactButton setTitle:@"குறைகள் / தொடர்புக்கு" forState:UIControlStateNormal];
        [self setTitle:@"தலை பக்கம்"];
        [_chooseLangageLabel setText:@"மொழி தேர்வு"];
        [self.imgView setImage:[UIImage imageNamed:@"bg_image_tamil"]];
    } else {
//        [_contactButton setTitle:@"GRIEVANCES / CONTACT" forState:UIControlStateNormal];
        [self setTitle:@"Home"];
        [_chooseLangageLabel setText:@"CHOOSE LANGUAGE"];
        [self.imgView setImage:[UIImage imageNamed:@"bg_image"]];
    }
}

- (IBAction)languageButtonAction:(UIButton *)sender {
    [self setIsTamilLanguage:[sender tag]];
    [self configureView];
}
   
@end
