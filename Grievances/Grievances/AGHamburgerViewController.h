//
//  AGHamburgerViewController.h
//  SystimaNX IT Solutions Pvt Ltd
//
//  Created by Rajesh on 10/24/15.
//  Copyright © 2015 SystimaNX IT Solutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BCViewController.h"

typedef enum {
    kHome,
    kGrievance,
    kWelfareScheme,
    kSuggestion
}AGHamburgerType;

@interface AGHamburgerViewController : BCViewController
+ (instancetype)sharedHamburger;
+ (void)showHamburger;
- (void)openHamburger:(BOOL)shouldOpen;
@end
