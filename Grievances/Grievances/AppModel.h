//
//
//  Created by Rajesh on 3/31/16.
//  Copyright © 2016 Rajesh. All rights reserved.
//

#import "MTLModel.h"
#import "MTLJSONAdapter.h"

@interface AppModel : MTLModel <MTLJSONSerializing>

+ (NSMutableDictionary *)propertyMapping;

+ (instancetype)fromJSON:(NSDictionary *)json;

+ (NSArray *)fromJSONArray:(NSArray *)json;

- (void)save;

+ (void)clear;

+ (void)flush; // To flush live memory

+ (instancetype)cached;

@end

@interface AGRecord : AppModel

@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSString *category;
@property (nonatomic, strong) NSString *ward;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *age;
@property (nonatomic, strong) NSString *constituencyName;
@property (nonatomic, strong) NSString *feedback;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *voterId;
@property (nonatomic, strong) NSString *sectionName;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *mobileNumber;

@property (nonatomic, strong) NSArray <NSString *> *constituencies;
@property (nonatomic, strong) NSArray *wards;
@property (nonatomic, strong) NSArray *sections;
@property (nonatomic, assign) BOOL otpVerified;

@end

@interface AGConstituency : AppModel

@property (nonatomic, strong) NSString *cId;
@property (nonatomic, strong) NSString *mlaPhoto;
@property (nonatomic, strong) NSString *constituencyNo;
@property (nonatomic, strong) NSString *remarks;
@property (nonatomic, strong) NSString *totalPopulation;
@property (nonatomic, strong) NSString *womenPopulation;
@property (nonatomic, strong) NSString *constituencyName;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *transgenderPopulation;
@property (nonatomic, strong) NSString *mlaName;
@property (nonatomic, strong) NSString *menPopulation;

@end

@interface AGWard : AppModel

@property (nonatomic, strong) NSString *wId;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *councillorName;
@property (nonatomic, strong) NSString *wardName;
@property (nonatomic, strong) NSString *constituency;
@property (nonatomic, strong) NSString *wardNo;

@end

@interface AGSection : AppModel

@property (nonatomic, strong) NSString *ward;
@property (nonatomic, strong) NSString *sectionNo;
@property (nonatomic, strong) NSString *sectionName;

@end
