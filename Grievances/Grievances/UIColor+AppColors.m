//
//  NSObject+FontUtil.m
//
//  Created by Rajesh on 1/16/16.
//

#import "UIColor+AppColors.h"

@implementation UIColor(AppColors)

+ (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+ (UIColor *)appRedColor {
    return [self colorFromHexString:@"#cc2245"];
}

+ (UIColor *)darkRedColor {
    return [self colorFromHexString:@"#ba455d"];
}

+ (UIColor *)darkBlueColor {
    return [self colorFromHexString:@"#2b2d42"];
}

+ (UIColor *)appGreenColor {
    return [self colorFromHexString:@"#125E2D"];
}

+ (UIColor *)silverColor {
    return [self colorFromHexString:@"#bdc3c7"];
}

+ (UIColor *)blackTextColor {
    return [self colorFromHexString:@"#212121"];
}

+ (UIColor *)secondaryTextColor {
    return [self colorFromHexString:@"#727272"];
}

+ (UIColor *)toastWarningColor {
    return [self colorFromHexString:@"#FF9500"];
}

+ (UIColor *)toastSuccessColor {
    return [self colorFromHexString:@"#1A907A"];
}

+ (UIColor *)toastErrorColor {
    return [self colorFromHexString:@"#E94638"];
}

+ (UIColor *)toastInfoColor {
    return [self colorFromHexString:@"#009DED"];
}

@end
