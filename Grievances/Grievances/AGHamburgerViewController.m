//
//  AGHamburgerViewController.m
//  SystimaNX IT Solutions Pvt Ltd
//
//  Created by Rajesh on 10/24/15.
//  Copyright © 2015 SystimaNX IT Solutions Pvt Ltd. All rights reserved.
//

#import "AGHamburgerViewController.h"
#import "APPModel.h"

@interface AGHamburgerCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *itemName;
@property (nonatomic, strong) IBOutlet UIImageView *itemIcon;
@property (nonatomic, weak) AGHamburgerViewController *owner;

@end

@implementation AGHamburgerCell

- (void)configureWithTitle:(NSString *)title type:(AGHamburgerType)type {
    [self.itemName setText:title];
    /*switch (type) {
        case  kHome :
            [self.itemIcon setImage:[UIImage imageNamed:@"ic_address"]];
            break;
        case kGrievance :
            [self.itemIcon setImage:[UIImage imageNamed:@"ic_profile"]];
            break;
        case kOrderHistory :
            [self.itemIcon setImage:[UIImage imageNamed:@"ic_order_history"]];
            break;
        case kFeedBack :
            [self.itemIcon setImage:[UIImage imageNamed:@"ic_feedback"]];
            break;
        case kOffers :
            [self.itemIcon setImage:[UIImage imageNamed:@"ic_offer"]];
            break;
        case kAboutUs :
            [self.itemIcon setImage:[UIImage imageNamed:@"ic_about"]];
            break;
        case kInvite :
            [self.itemIcon setImage:[UIImage imageNamed:@"ic_invite"]];
            break;
        case kLogout :
            [self.itemIcon setImage:[UIImage imageNamed:@"ic_logout"]];
            break;
        default:
            break;
    }
     */
}

@end
@interface AGHamburgerViewController () <UIGestureRecognizerDelegate> {
    NSMutableArray *arrayOfItems;
    NSMutableArray *arrayOfTamilItems;
    BOOL shouldRefreshCanvas;
}

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (strong, nonatomic) UIPanGestureRecognizer *panGesture;

@end

@implementation AGHamburgerViewController
+ (instancetype)sharedHamburger{
    static AGHamburgerViewController *hamburgerViewController;
    if (!hamburgerViewController) {
        hamburgerViewController = [self instance];
        __block CGRect frame = hamburgerViewController.view.frame;
            frame.origin.x = - hamburgerViewController.view.frame.size.width;
        [hamburgerViewController.view setFrame:frame];
        [[UIApplication sharedApplication].keyWindow addSubview:hamburgerViewController.view];
    }
    return hamburgerViewController;
}

+ (void)showHamburger {
    [[self sharedHamburger] openHamburger:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    arrayOfItems = [NSMutableArray arrayWithObjects:@"Home", @"Grievance", @"Welfare Scheme",@"Suggestion", nil];
    arrayOfTamilItems = [NSMutableArray arrayWithObjects:@"தலை பக்கம்", @"குறைகள்", @"பொதுநல திட்டம்",@"பரிந்துரை", nil];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
    [tapGesture addTarget:self action:@selector(handleGesture:)];
    [tapGesture setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:tapGesture];
    
    _panGesture = [[UIPanGestureRecognizer alloc] init];
    [_panGesture setDelegate:self];
    [_panGesture setCancelsTouchesInView:NO];
    [_panGesture addTarget:self action:@selector(handleGesture:)];
    [self.view addGestureRecognizer:_panGesture];
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    if (gestureRecognizer == _panGesture) {
        CGPoint translation = [(UIPanGestureRecognizer *)gestureRecognizer translationInView:self.view];
        return (fabs(translation.x) / fabs(translation.y) > 1) ? YES : NO;
    }
    return YES;
}

- (void)handleGesture:(UIGestureRecognizer *)gestureRecognizer {
    CGPoint point = [gestureRecognizer locationInView:self.view];
    if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
            UIPanGestureRecognizer *panGesture = (UIPanGestureRecognizer *)gestureRecognizer;
            CGPoint pointVelocity = [panGesture velocityInView:self.view];
            CGPoint pointLocation = [panGesture locationInView:self.view];
            static CGPoint previousLocation;
            if (gestureRecognizer.state == UIGestureRecognizerStateChanged) {
                    CGRect frame = self.bgView.frame;
                CGFloat newX = frame.origin.x - (previousLocation.x - pointLocation.x);
                    if (newX < 0) {
                        frame.origin.x = newX;
                        [self.bgView setFrame:frame];
                }
            }
            else if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
                __block CGRect frame = self.bgView.frame;
                if (pointVelocity.x > 0) {
                    frame.origin.x = 0;
                    [UIView animateWithDuration:.2 animations:^{
                        [self.bgView setFrame:frame];
                    }];
                }else{
                    frame.origin.x = -frame.size.width;
                    [UIView animateWithDuration:.3 animations:^{
                        [self.view setBackgroundColor:[UIColor clearColor]];
                    }];
                    [UIView animateWithDuration:.3 animations:^{
                        [self.bgView setFrame:frame];
                    } completion:^(BOOL finished) {
                        frame = self.view.frame;
                        frame.origin.x = -frame.size.width;
                        [self.view setFrame:frame];
                    }];
                }

            }
            previousLocation = pointLocation;
    }
    else if (CGRectContainsPoint(CGRectMake(self.view.bounds.size.width * 3/4, self.view.bounds.origin.y,self.view.bounds.size.width  - self.view.bounds.size.width * 3/4, self.view.bounds.size.height), point)) {
        [self openHamburger:NO];
    }
}
/*
- (IBAction)profileAction:(id)sender {
    [self openHamburger:NO];
    [BCNavigationController hamburgerActionReceived:kMainPage];
}
*/

- (void)openHamburger:(BOOL)shouldOpen{
    __block CGRect frame = self.view.frame;
    if (shouldOpen){
        [self.tableView reloadData];
        [self.view.superview bringSubviewToFront:self.view];
        frame.origin.x = - self.view.frame.size.width;
        [self.view setFrame:frame];
        [UIView animateWithDuration:.4 animations:^{
            frame.origin.x = 0;
            [self.view setFrame:frame];
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:.2 animations:^{
                [self.view setBackgroundColor:[UIColor colorWithWhite:.0 alpha:.4]];
            }];
        }];
    }else{
        frame.origin.x = 0;
        [self.view setFrame:frame];
        [UIView animateWithDuration:.1 animations:^{
            [self.view setBackgroundColor:[UIColor clearColor]];
        }];
        [UIView animateWithDuration:.4 animations:^{
            frame.origin.x = - self.view.frame.size.width;
            [self.view setFrame:frame];
        }];
    }
}

#pragma mark - UITableViewDataSource methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrayOfItems count];
}

- (AGHamburgerCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AGHamburgerCell *hamburgerCell =  [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([AGHamburgerCell class]) forIndexPath:indexPath];
    [hamburgerCell configureWithTitle:self.isTamilLanguage ? arrayOfTamilItems[indexPath.row] : arrayOfItems[indexPath.row]  type:(AGHamburgerType)indexPath.row];
    return hamburgerCell;
}

#pragma mark - UITableViewDetegate method
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self openHamburger:NO];
    [BCNavigationController hamburgerActionReceived:indexPath.row];
}

@end
