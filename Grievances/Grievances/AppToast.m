//
// Created by Rajesh on 17/04/16.
//

#import "AppToast.h"
#import <CRToast/CRToast.h>
#import "UIColor+AppColors.h"

@implementation AppToast

+ (void)showMessage:(NSString *)message {
    NSMutableDictionary *options = (NSMutableDictionary *) [@{
                                                              kCRToastTextKey : message,
                                                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                                                              kCRToastFontKey:[UIFont systemFontOfSize:13],
                                                              kCRToastBackgroundColorKey : [UIColor toastInfoColor],
                                                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeLinear),
                                                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeLinear),
                                                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                                                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop),
                                                              kCRToastTimeIntervalKey : @(2),
                                                              kCRToastNotificationTypeKey :@(CRToastTypeStatusBar),
                                                            } mutableCopy];
    dispatch_async(dispatch_get_main_queue(), ^{
        [CRToastManager showNotificationWithOptions:options
                                    completionBlock:nil];
    });
}

+ (void)showMessage:(NSString *)message type:(KCToastType)type {
    NSMutableDictionary *options = (NSMutableDictionary *) [@{
                                                              kCRToastTextKey : message,
                                                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                                                              kCRToastFontKey:[UIFont systemFontOfSize:13],
                                                              kCRToastBackgroundColorKey : (type == kToastTypeError ? [UIColor toastErrorColor] : (type == kToastTypeWarning ? [UIColor toastWarningColor] : [UIColor toastSuccessColor])),
                                                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeLinear),
                                                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeLinear),
                                                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                                                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop),
                                                              kCRToastTimeIntervalKey : @(2),
                                                              kCRToastNotificationTypeKey :@(CRToastTypeStatusBar),
                                                              } mutableCopy];
    dispatch_async(dispatch_get_main_queue(), ^{
        [CRToastManager showNotificationWithOptions:options
                                    completionBlock:nil];
    });
}

+ (void)showNotification:(NSString *)title message:(NSString *)message completionBlock:(void (^)(void))completion {
    NSMutableDictionary *options = (NSMutableDictionary *) [@{
                                                              kCRToastTextKey : title,
                                                              kCRToastTextAlignmentKey : @(NSTextAlignmentLeft),
                                                              kCRToastSubtitleTextKey : message,
                                                              kCRToastSubtitleTextAlignmentKey : @(NSTextAlignmentLeft),
                                                              kCRToastFontKey:[UIFont systemFontOfSize:13],
                                                              kCRToastBackgroundColorKey : [UIColor colorWithWhite:.1f alpha:.9f],
                                                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeLinear),
                                                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeLinear),
                                                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                                                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop),
                                                              kCRToastTimeIntervalKey : @(2),
                                                              kCRToastNotificationTypeKey :@(CRToastTypeNavigationBar),
                                                              kCRToastInteractionRespondersKey : @[
                                                                                                   [CRToastInteractionResponder interactionResponderWithInteractionType:CRToastInteractionTypeTapOnce automaticallyDismiss:YES block:^(CRToastInteractionType interactionType) {
                                                                                                            completion();
                                                                                                    }]]} mutableCopy];
    dispatch_async(dispatch_get_main_queue(), ^{
        [CRToastManager showNotificationWithOptions:options
                                    completionBlock:nil];
    });
}

+ (void)dismiss {
    [CRToastManager dismissNotification:NO];
}

@end