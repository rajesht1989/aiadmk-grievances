//
// Created by Rajesh on 17/04/16.
//


#import <CoreGraphics/CoreGraphics.h>

@interface AppToast : NSObject

typedef enum {
    kToastTypeError,
    kToastTypeWarning,
    kToastTypeSuccess,
}KCToastType;

+ (void)showMessage:(NSString *)message;
+ (void)showMessage:(NSString *)message type:(KCToastType)type;
+ (void)showNotification:(NSString *)title message:(NSString *)message completionBlock:(void (^)(void))completion;
+ (void)dismiss;

@end