//
//
//  Created by Rajesh on 3/31/16.
//  Copyright © 2016 Rajesh. All rights reserved.
//

#import "AppAPI.h"
#import "AppAPIClient.h"

@implementation AppAPI

+ (NSString *)normalizeParam:(NSString *)param {
    if (!param) {
        param = @"";
    }
    return param;
}

+ (AFPromise *)constituencies {
    return [AppAPIClient POST:[AppAPIClient endpoint:@"constituency"] parameters:nil].then(^(NSDictionary* response) {
        return [AGConstituency fromJSONArray:response[@"data"]];
    });
}

+ (AFPromise *)wardsOfConstituency:(NSString *)constituency {
    return [AppAPIClient POST:[AppAPIClient endpoint:@"ward"] parametersAsFormData:@{@"constituency_no" : constituency}].then(^(NSDictionary* response) {
        return [AGWard fromJSONArray:response[@"data"]];
    });
}

+ (AFPromise *)sectionsOfWard:(NSString *)ward {
    return [AppAPIClient POST:[AppAPIClient endpoint:@"section"] parametersAsFormData:@{@"ward_no" : ward}].then(^(NSDictionary* response) {
        return [AGSection fromJSONArray:response[@"data"]];
    });
}

+ (AFPromise *)sendOtp:(NSString *)mobile {
    return [AppAPIClient POST:[AppAPIClient endpoint:@"send_otp"] parametersAsFormData:@{@"mobile" : mobile}].then(^(NSDictionary* response) {
        return response;
    });
}

+ (AFPromise *)verifyOtp:(NSString *)otp {
    return [AppAPIClient POST:[AppAPIClient endpoint:@"verify_otp"] parametersAsFormData:@{@"otp" : otp}].then(^(NSDictionary* response) {
        return response;
    });
}

+ (AFPromise *)addRecord:(AGRecord *)record {
    NSDictionary *param = @{
                            @"name" : [self normalizeParam:record.name],
                            @"age" : [self normalizeParam:record.age],
                            @"gender" : [self normalizeParam:record.gender],
                            @"voter_id" : [self normalizeParam:record.voterId],
                            @"address" : [self normalizeParam:record.address],
                            @"ward" : [self normalizeParam:record.ward],
                            @"section_name" : [self normalizeParam:record.sectionName],
                            @"constituency_name" : [self normalizeParam:record.constituencyName],
                            @"mobile_number" : [self normalizeParam:record.mobileNumber],
                            @"feedback" : [self normalizeParam:record.feedback],
                            @"images" : @"",
                            @"video_files" : @"",
                            @"imei" : [[[UIDevice currentDevice] identifierForVendor] UUIDString]
                            };
    return [AppAPIClient POST:[AppAPIClient endpoint:@"user_grievance"] parametersAsFormData:param].then(^(NSDictionary* response) {
        return response;
    });
}

@end