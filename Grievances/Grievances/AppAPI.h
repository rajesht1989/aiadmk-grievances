//
//
//  Created by Rajesh on 3/31/16.
//  Copyright © 2016 Rajesh. All rights reserved.
//

#import "AFNetworking+PromiseKit.h"
#import "AppAPIClient.h"
#import "AppModel.h"

@interface AppAPI : NSObject

+ (AFPromise *)constituencies;
+ (AFPromise *)wardsOfConstituency:(NSString *)constituency;
+ (AFPromise *)sectionsOfWard:(NSString *)ward;
+ (AFPromise *)sendOtp:(NSString *)mobile;
+ (AFPromise *)verifyOtp:(NSString *)otp;
+ (AFPromise *)addRecord:(AGRecord *)record;

@end
