//
//  GrievancesViewController.m
//  Grievances
//
//  Created by Rajesh on 6/23/16.
//  Copyright © 2016 Org. All rights reserved.
//

#import "AGWelfareViewController.h"
#import "UIColor+AppColors.h"

typedef enum {
    kVoterId = 0,
    kName,
    kEmail,
    kAge,
    kGender,
    kAddress,
    kConstituency,
    kWard,
    kSection,
    kWelfare,
    kMobile,
    kVerify,
    kFeedback
}AGFieldType;

@interface AGWelfareViewController ()

@property (strong, nonatomic) IBOutlet UIView *tableFooter;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (strong, nonatomic) AGRecord *record;
@property (strong, nonatomic) NSArray <AGConstituency *>* constituencies;

- (void)constituencyOfIndexSelected:(NSInteger)index;
- (void)wardOfIndexSelected:(NSInteger)index;

@end

@interface AGWelfareCell : UITableViewCell <UITextFieldDelegate, UITextViewDelegate>

@property(nonatomic,weak) UILabel *titleLabel;
@property(nonatomic,weak) UITextField *textField;
@property(nonatomic,weak) UITextView *textView;
@property (weak, nonatomic) IBOutlet UIButton *otpBtn;
@property (weak, nonatomic) IBOutlet UIButton *maleBtn;
@property (weak, nonatomic) IBOutlet UIButton *femaleBtn;
@property (weak, nonatomic) IBOutlet UIButton *transgenderedBtn;
@property (weak, nonatomic) AGWelfareViewController *owner;
@property (assign, nonatomic) BOOL isCellConfiguredForLanguage;
@property (assign, nonatomic) AGFieldType type;

@end

@implementation AGWelfareCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _titleLabel = [self.contentView viewWithTag:1];
    _textField = [self.contentView viewWithTag:2];
    _textView = [self.contentView viewWithTag:3];
    
    if (_textField) {
        _textField.delegate = self;
    }
    
    if (_otpBtn) {
        [_otpBtn.titleLabel setNumberOfLines:0];
        [_otpBtn.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [_otpBtn.layer setCornerRadius:3.f];
    }
    
    if (_textView) {
        [_textView.layer setBorderColor:[[UIColor colorFromHexString:@"C1C1C1"] CGColor]];
        [_textView.layer setBorderWidth:.5f];
        [_textView.layer setCornerRadius:4.f];;
        _textView.delegate = self;
    }
}

- (void)configureCellWith:(NSInteger)index {
    _type = (AGFieldType)index;
    if (!_isCellConfiguredForLanguage) {
        if (_owner.isTamilLanguage) {
            switch (index) {
                case kName :
                    [_titleLabel setText:@"பெயர்"];
                    break;
                case kAge :
                    [_titleLabel setText:@"வயது"];
                    break;
                case kGender :
                    [_titleLabel setText:@"பாலினம்"];
                    [_maleBtn setTitle:@"ஆண்" forState:UIControlStateNormal];
                    [_femaleBtn setTitle:@"பெண்" forState:UIControlStateNormal];
                    [_transgenderedBtn setTitle:@"திருநங்கை" forState:UIControlStateNormal];
                    break;
                case kVoterId :
                    [_titleLabel setText:@"வாக்காளர் அட்டை எண்"];
                    break;
                case kAddress :
                    [_titleLabel setText:@"முகவரி"];
                    break;
                case kWard :
                    [_titleLabel setText:@"வார்டு"];
                    break;
                case kSection :
                    [_titleLabel setText:@"பிரிவின் பெயர்"];
                    break;
                case kConstituency :
                    [_titleLabel setText:@"தொகுதியின் பெயர்"];
                    break;
                case kMobile :
                    [_titleLabel setText:@"தொலைபேசி எண்"];
                    [_otpBtn setTitle:@"சரிபார்க்கவும்\n(GENERATE OTP)" forState:UIControlStateNormal];
                    break;
                case kFeedback :
                    [_titleLabel setText:@"குறைகள்"];
                    break;
                case  kEmail :
                    [_titleLabel setText:@"மின்னஞ்சல்"];
                    break;
                case  kWelfare :
                    [_titleLabel setText:@"பொதுநல திட்டம்"];
                    break;
                case kVerify :
                    [_titleLabel setText:@"OTP சரிபார்க்க"];
                    break;
                default:
                    break;
            }
        } else {
            switch (index) {
                case kName :
                    [_titleLabel setText:@"NAME"];
                    break;
                case kAge :
                    [_titleLabel setText:@"AGE"];
                    break;
                case kGender :
                    [_titleLabel setText:@"GENDER"];
                    [_maleBtn setTitle:@"MALE" forState:UIControlStateNormal];
                    [_femaleBtn setTitle:@"FEMALE" forState:UIControlStateNormal];
                    [_transgenderedBtn setTitle:@"TRANSGENDERED" forState:UIControlStateNormal];
                    break;
                case kVoterId :
                    [_titleLabel setText:@"VOTER ID NO"];
                    break;
                case kAddress :
                    [_titleLabel setText:@"ADDRESS"];
                    break;
                case kWard :
                    [_titleLabel setText:@"WARD"];
                    break;
                case kSection :
                    [_titleLabel setText:@"SECTION NAME"];
                    break;
                case kConstituency :
                    [_titleLabel setText:@"CONSTITUENCY NAME"];
                    break;
                case kMobile :
                    [_titleLabel setText:@"MOBILE NUMBER"];
                    [_otpBtn setTitle:@"CHECK\n(GENERATE OTP)" forState:UIControlStateNormal];
                    break;
                case kFeedback :
                    [_titleLabel setText:@"FEEDBACK"];
                    break;
                case  kEmail :
                    [_titleLabel setText:@"EMAIL"];
                    break;
                case  kWelfare :
                    [_titleLabel setText:@"WELFARE SCHEME"];
                    break;
                case kVerify :
                    [_titleLabel setText:@"VERIFY OTP"];
                    break;
                default:
                    break;
            }
        }
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@*",_titleLabel.text]];
        NSRange range = NSMakeRange(_titleLabel.text.length, 1);
        [attributedString addAttribute:NSForegroundColorAttributeName
                                 value:[UIColor redColor]
                                 range:range];
        
        [_titleLabel setAttributedText:attributedString];
        _isCellConfiguredForLanguage = !_isCellConfiguredForLanguage;
    }
        switch (index) {
            case kName :
                [_textField setText:_owner.record.name];
                break;
            case kAge :
                [_textField setText:_owner.record.age];
                break;
            case kGender :
                if (_owner.record.gender.length == 0) [self genderButtonAction:nil];
                break;
            case kVerify :
                if (!_owner.record.otpVerified) [_textField setText:nil];
                break;
            case kVoterId :
                [_textField setText:_owner.record.voterId];
                break;
            case kAddress :
                [_textView setText:_owner.record.address];
                break;
            case kWard :
                [_textField setText:_owner.record.ward];
                break;
            case kSection :
                [_textField setText:_owner.record.sectionName];
                break;
            case kConstituency :
                [_textField setText:_owner.record.constituencyName];
                break;
            case kMobile :
                [_textField setText:_owner.record.mobileNumber];
                break;
            case kFeedback :
                [_textView setText:_owner.record.feedback];
                break;
            case  kEmail :
                [_textField setText:_owner.record.email];
                break;
            case  kWelfare :
                [_textField setText:_owner.record.category]; //TODO
                break;
            default:
                break;
        }
}

- (IBAction)checkButtonAction:(id)sender {
    [_owner showLoadingWithMessage:nil];
    [AppAPI sendOtp:_textField.text].then(^(NSDictionary *response) {
    }).catch(^(NSError *error){
        [AppToast showMessage:error.userInfo[NSLocalizedDescriptionKey]];
    }).finally(^{
        [_owner hideLoading];
    });
}

- (IBAction)genderButtonAction:(id)sender {
    _maleBtn.selected = _femaleBtn.selected = _transgenderedBtn.selected = NO;
    [sender setSelected:YES];
    [_owner.record setGender:[sender titleForState:UIControlStateNormal]];
}

- (NSString *)cellTitle {
    return [_titleLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"*"]];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    switch (_type) {
        case kConstituency : {
            if (_owner.record.constituencies == 0) return NO;
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[self cellTitle] message:nil preferredStyle:UIAlertControllerStyleActionSheet];
            for (NSString *constituency in _owner.record.constituencies) {
                [alertController addAction:[UIAlertAction actionWithTitle:constituency style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [_textField setText:action.title];
                    [_owner constituencyOfIndexSelected:[_owner.record.constituencies indexOfObject:constituency]];
                }]];
            }
            [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
            [_owner presentViewController:alertController animated:YES completion:nil];
            return NO;
        }
            break;
        case kWard :{
            if (_owner.record.wards == 0) return NO;
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[self cellTitle] message:nil preferredStyle:UIAlertControllerStyleActionSheet];
            for (AGWard *aWard in _owner.record.wards) {
                [alertController addAction:[UIAlertAction actionWithTitle:aWard.wardName style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [_textField setText:action.title];
                    [_owner wardOfIndexSelected:[_owner.record.wards indexOfObject:aWard]];
                }]];
            }
            [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
            [_owner presentViewController:alertController animated:YES completion:nil];
            return NO;
        }
            break;
        case kSection :{
            if (_owner.record.sections == 0) return NO;
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[self cellTitle] message:nil preferredStyle:UIAlertControllerStyleActionSheet];
            for (AGSection *aSection in _owner.record.sections) {
                [alertController addAction:[UIAlertAction actionWithTitle:aSection.sectionName style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [_textField setText:action.title];
                    _owner.record.sectionName = _textField.text;
                }]];
            }
            [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
            [_owner presentViewController:alertController animated:YES completion:nil];
            return NO;
        }
            break;
        case kWelfare :{//TODO
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[self cellTitle] message:nil preferredStyle:UIAlertControllerStyleActionSheet];
            NSArray *categories = _owner.isTamilLanguage ? @[@"10ஆம் வகுப்பிற்கு மேற்பட்ட படிப்பிற்கான (post matric) மாநில அரசின் சிறப்பு உதவித் தொகை திட்டம்",@"10ஆம் வகுப்பிற்கு மேற்பட்ட படிப்பிற்கான (post matric) மைய அரசு உதவித் தொகை திட்டம்.",@"10ஆம் வகுப்பு (மூன்று பரிசுகள்)",@"10ம் வகுப்பு மாநில அளவில் பாடங்களில் முதன்மை (5 பாடங்கள்)",@"12ம் வகுப்பில் மாநில அளவில் பாடங்களில் முதன்மை (25 பாடங்கள்)",@"12ம் வகுப்பு வரை அனைவருக்கும் இலவச உணவு உறைவிடம்",@"14 வயதுக்கு மேற்பட்ட மனவளர்ச்சி குன்றியோருக்கான இல்லம் அமைத்தல்",@"7. சட்டக் கல்வி படித்த மாற்றுத் திறனாளிகளுக்கு உதவித் தொகை",@"அக்மார்க் தரம் பிரிப்பு",@"அங்ககச்சான்றளிப்பு",@"அயல்நாடு சென்று உயர் கல்விப் பயில உதவித்தொகை",@"அரசு ஏழைகள் பராமரிப்பு இல்லம் மேல்பாக்கம்",@"அரசு மறுவாழ்வு இல்லங்கள்",@"ஆட்டோ கடன் திட்டம்",@"ஆதரவின்றி சுற்றித் திரியும் மன நலம் பாதிக்கப்பட்டோரை மீட்டு மறுவாழ்வு மையங்களில் சேர்க்கும் மீட்புத் திட்டம்",@"இணைப்புச் சாலை",@"இணைப்புச் சாலைகள் அமைத்தல்"] :   @[@"Abolition of bonded labour system",@"Abolition of bonded labour system for sc/st",@"Accident relief scheme",@"Additional capital subsidy for women, sc / st, differently abled and transgender entrepreneurs",@"Additional capital subsidy to promote cleaner and environment friendly technologies",@"Adi dravidar and tribal welfare department -hostels - special guides",@"Adi dravidar and tribal welfare department -incentive / award of prizes - district level prize plus2 examination rs.3000/- 10th std first prize rs.1000/- second prize rs.500/- third prize rs.300/.",@"Adi dravidar and tribal welfare department -incentive / award of prizes - for each subjects (c) plus2 examination rs.2000/- (d)10th std examination rs.1000/-",@"Adi dravidar and tribal welfare department -scholarship - free education upto 12th std. to all i.e. tuition fee will not be collected and the amount will be reimbursed by government.",@"Adi dravidar and tribal welfare department -scholarship - public examination fee for 10th and 12th std.",@"Adi dravidar and tribal welfare department - stationary - text books",@"Admission of adi dravidar /tribal /adi dravidar converted to christianity students in reputed residential school in vi std",@"Admission of adi dravidar / tribal students in plusone at reputed schools.",@"Adoption",@"Afforestation programmes",@"Afforestation schemes providing incentives and providing employment to tribals in forest operation.",@"Agmark grading",@"Agricultural mechanisation programme",@"Agricultural producers cooperative marketing society",@"Agriculture input subsidy where crop loss is 50 percent and above",@"Agriculture technology management agency ( atma )",@"Agriculture technology management agency - training of farmers",@"All india service examinations like i.a.s., i.p.s., i.r.s., etc.",@"Animal husbandry",@"Animal husbandry - tribal welfare schemes"];
            for (NSString *aCategory in categories) {
                [alertController addAction:[UIAlertAction actionWithTitle:aCategory style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [_textField setText:action.title];
                    _owner.record.category = _textField.text;
                }]];
            }
            [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
            [_owner presentViewController:alertController animated:YES completion:nil];
            return NO;
        }
            break;
        default:
            return YES;
            break;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    switch (_type) {
        case  kVoterId :
            _owner.record.voterId = textField.text;
            break;
        case kName :
            _owner.record.name = textField.text;
            break;
        case kEmail :
            _owner.record.email = textField.text;
            break;
        case kAge :
            _owner.record.age = textField.text;
            break;
        case kMobile :
            _owner.record.mobileNumber = textField.text;
            break;
        case kVerify : {
            [_owner showLoadingWithMessage:nil];
            [AppAPI verifyOtp:textField.text].then(^(NSDictionary *response){
                _owner.record.otpVerified = YES;
            }).catch(^(NSError *error){
                [AppToast showMessage:error.userInfo[NSLocalizedDescriptionKey]];
            }).finally(^{
                [_owner hideLoading];
            });
        }
            break;
        default:
            break;
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    switch (_type) {
        case kAddress :
            _owner.record.address = textView.text;
            break;
        case kFeedback :
            _owner.record.feedback = textView.text;
            break;
        default:
            break;
    }
}

@end

@implementation AGWelfareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureHamburgerButton];
    [self.tableView setTableFooterView:_tableFooter];

#if DEBUG
    [self fillDefaultValues];
#endif
    
    if (self.isTamilLanguage) {
        [_saveButton setTitle:@"சேமிக்க" forState:UIControlStateNormal];
        [self setTitle:@"பொதுநல திட்டம்"];
    } else {
        [_saveButton setTitle:@"SAVE" forState:UIControlStateNormal];
        [self setTitle:@"WELFARE SCHEME"];
    }
    [AppAPI constituencies].then(^(NSArray <AGConstituency *>*constituencies){
        _constituencies = constituencies;
        _record.constituencies = [_constituencies valueForKey:@"constituencyName"];
    });
}

- (void)fillDefaultValues {
    [self setRecord:[AGRecord fromJSON:@{@"name":@"test",@"age":@"22",@"voter_id":@"voterid",@"address":@"test ",@"mobile_number":@"9578353705",@"feedback":@"Jkasjkajk"}]];
}

- (AGRecord *)record {
    if (!_record) {
        _record = [AGRecord new];
        _record.constituencies = [_constituencies valueForKey:@"constituencyName"];
    }
    return _record;
}

- (IBAction)saveButtonAction:(id)sender {
    _record.otpVerified = YES;
    if (_record.otpVerified) {
        [self.view endEditing:YES];
        [self showLoadingWithMessage:self.isTamilLanguage ? @"சேமிக்கப்படுகிறது" : @"Saving"];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self hideLoading];
            [self setRecord:nil];
            [self.tableView reloadData];
            if (self.isTamilLanguage) {
                [self showMessage:@"வெற்றிகரமாக சேமிக்கப்பட்டது"];
            } else {
                [self showMessage:@"SAVED SUCCESSFULLY"];
            }
        });
        return;
        /*
        [AppAPI addRecord:_record].then(^{
            if (self.isTamilLanguage) {
                [self showMessage:@"வெற்றிகரமாக சேமிக்கப்பட்டது"];
            } else {
                [self showMessage:@"SAVED SUCCESSFULLY"];
            }
            [self setRecord:nil];
            [self.tableView reloadData];
        }).catch(^(NSError *error){
            [AppToast showMessage:error.userInfo[NSLocalizedDescriptionKey]];
        }).finally(^{
            [self hideLoading];
        });
         */
    } else {
        if (self.isTamilLanguage) {
            [self showMessage:@"நீங்கள் சேமிக்கும் முன் OTP சரிபார்க்க வேண்டும்"];
        } else {
            [self showMessage:@"You need to verify OTP before saving"];
        }
    }
}

// Cell action
- (void)constituencyOfIndexSelected:(NSInteger)index {
    AGConstituency *constituency = _constituencies[index];
    _record.constituencyName = constituency.constituencyName;
    [self showLoadingWithMessage:nil];
    [AppAPI wardsOfConstituency:constituency.cId].then(^(NSArray *wards){
        _record.wards = wards;
    }).catch(^(NSError *error){
        [AppToast showMessage:error.userInfo[NSLocalizedDescriptionKey]];
    }).finally(^{
        [self hideLoading];
    });
}

- (void)wardOfIndexSelected:(NSInteger)index {
    AGWard *ward = _record.wards[index];
    _record.ward = ward.wardName;
    [self showLoadingWithMessage:nil];
    [AppAPI sectionsOfWard:ward.wId].then(^(NSArray *sections){
        _record.sections = sections;
    }).catch(^(NSError *error){
        [AppToast showMessage:error.userInfo[NSLocalizedDescriptionKey]];
    }).finally(^{
        [self hideLoading];
    });
}

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return kFeedback + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AGWelfareCell *cell = [tableView dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"%@%d",NSStringFromClass([AGWelfareCell class]),(int)indexPath.row] forIndexPath:indexPath];
    [cell setOwner:self];
    [cell configureCellWith:indexPath.row];
    return cell;
}

#pragma mark UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case kAddress :
        case kFeedback :
            return 100;
            break;
        case kGender:
            return 120;
            break;
        default:
            return 60;
            break;
    }
}

@end

