//
//  AppDelegate.h
//  Grievances
//
//  Created by Rajesh on 6/22/16.
//  Copyright © 2016 Org. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

