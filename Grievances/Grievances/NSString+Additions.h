//
//  Created by Rajesh on 3/26/16.
//  Copyright © 2016 Rajesh. All rights reserved.
//



@interface NSString (Additions)

+ (NSString*)paramString:(NSDictionary *)dictionary;

- (NSString *)camelCaseToUnderscores;

- (NSString *)underscoresToCamelCase;

- (NSString *)urlEncode;

- (BOOL)isValidEmail;

@end