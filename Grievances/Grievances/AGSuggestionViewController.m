//
//  GrievancesViewController.m
//  Grievances
//
//  Created by Rajesh on 6/23/16.
//  Copyright © 2016 Org. All rights reserved.
//

#import "AGSuggestionViewController.h"
#import "UIColor+AppColors.h"

typedef enum {
    kVoterId = 0,
    kName,
    kEmail,
    kConstituency,
    kWard,
    kMobile,
    kVerify,
    kSuggession
}AGFieldType;

@interface AGSuggestionViewController ()

@property (strong, nonatomic) IBOutlet UIView *tableFooter;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (strong, nonatomic) AGRecord *record;
@property (strong, nonatomic) NSArray <AGConstituency *>* constituencies;

- (void)constituencyOfIndexSelected:(NSInteger)index;
- (void)wardOfIndexSelected:(NSInteger)index;

@end

@interface AGSuggestionCell : UITableViewCell <UITextFieldDelegate, UITextViewDelegate>

@property(nonatomic,weak) UILabel *titleLabel;
@property(nonatomic,weak) UITextField *textField;
@property(nonatomic,weak) UITextView *textView;
@property (weak, nonatomic) IBOutlet UIButton *otpBtn;
@property (weak, nonatomic) IBOutlet UIButton *maleBtn;
@property (weak, nonatomic) IBOutlet UIButton *femaleBtn;
@property (weak, nonatomic) IBOutlet UIButton *transgenderedBtn;
@property (weak, nonatomic) AGSuggestionViewController *owner;
@property (assign, nonatomic) BOOL isCellConfiguredForLanguage;
@property (assign, nonatomic) AGFieldType type;

@end

@implementation AGSuggestionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _titleLabel = [self.contentView viewWithTag:1];
    _textField = [self.contentView viewWithTag:2];
    _textView = [self.contentView viewWithTag:3];
    
    if (_textField) {
        _textField.delegate = self;
    }
    
    if (_otpBtn) {
        [_otpBtn.titleLabel setNumberOfLines:0];
        [_otpBtn.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [_otpBtn.layer setCornerRadius:3.f];
    }
    
    if (_textView) {
        [_textView.layer setBorderColor:[[UIColor colorFromHexString:@"C1C1C1"] CGColor]];
        [_textView.layer setBorderWidth:.5f];
        [_textView.layer setCornerRadius:4.f];;
        _textView.delegate = self;
    }
}

- (void)configureCellWith:(NSInteger)index {
    _type = (AGFieldType)index;
    if (!_isCellConfiguredForLanguage) {
        if (_owner.isTamilLanguage) {
            switch (index) {
                case kName :
                    [_titleLabel setText:@"பெயர்"];
                    break;
                case kVoterId :
                    [_titleLabel setText:@"வாக்காளர் அட்டை எண்"];
                    break;
                case kWard :
                    [_titleLabel setText:@"வார்டு"];
                    break;
                case kConstituency :
                    [_titleLabel setText:@"தொகுதியின் பெயர்"];
                    break;
                case kMobile :
                    [_titleLabel setText:@"தொலைபேசி எண்"];
                    [_otpBtn setTitle:@"சரிபார்க்கவும்\n(GENERATE OTP)" forState:UIControlStateNormal];
                    break;
                case kSuggession :
                    [_titleLabel setText:@"பரிந்துரை"];
                    break;
                case  kEmail :
                    [_titleLabel setText:@"மின்னஞ்சல்"];
                    break;
                case kVerify :
                    [_titleLabel setText:@"OTP சரிபார்க்க"];
                    break;
                default:
                    break;
            }
        } else {
            switch (index) {
                case kName :
                    [_titleLabel setText:@"NAME"];
                    break;
                case kVoterId :
                    [_titleLabel setText:@"VOTER ID NO"];
                    break;
                case kWard :
                    [_titleLabel setText:@"WARD"];
                    break;
                case kConstituency :
                    [_titleLabel setText:@"CONSTITUENCY NAME"];
                    break;
                case kMobile :
                    [_titleLabel setText:@"MOBILE NUMBER"];
                    [_otpBtn setTitle:@"CHECK\n(GENERATE OTP)" forState:UIControlStateNormal];
                    break;
                case kSuggession :
                    [_titleLabel setText:@"SUGGESSION"];
                    break;
                case  kEmail :
                    [_titleLabel setText:@"EMAIL"];
                    break;
                case kVerify :
                    [_titleLabel setText:@"VERIFY OTP"];
                    break;
                default:
                    break;
            }
        }
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@*",_titleLabel.text]];
        NSRange range = NSMakeRange(_titleLabel.text.length, 1);
        [attributedString addAttribute:NSForegroundColorAttributeName
                                 value:[UIColor redColor]
                                 range:range];
        
        [_titleLabel setAttributedText:attributedString];
        _isCellConfiguredForLanguage = !_isCellConfiguredForLanguage;
    }
        switch (index) {
            case kName :
                [_textField setText:_owner.record.name];
                break;
            case kVerify :
                if (!_owner.record.otpVerified) [_textField setText:nil];
                break;
            case kVoterId :
                [_textField setText:_owner.record.voterId];
                break;
            case kWard :
                [_textField setText:_owner.record.ward];
                break;
            case kConstituency :
                [_textField setText:_owner.record.constituencyName];
                break;
            case kMobile :
                [_textField setText:_owner.record.mobileNumber];
                break;
            case kSuggession :
                [_textView setText:_owner.record.feedback];
                break;
            case  kEmail :
                [_textField setText:_owner.record.email];
                break;
            default:
                break;
        }
}

- (IBAction)checkButtonAction:(id)sender {
    [_owner showLoadingWithMessage:nil];
    [AppAPI sendOtp:_textField.text].then(^(NSDictionary *response) {
    }).catch(^(NSError *error){
        [AppToast showMessage:error.userInfo[NSLocalizedDescriptionKey]];
    }).finally(^{
        [_owner hideLoading];
    });
}

- (IBAction)genderButtonAction:(id)sender {
    _maleBtn.selected = _femaleBtn.selected = _transgenderedBtn.selected = NO;
    [sender setSelected:YES];
    [_owner.record setGender:[sender titleForState:UIControlStateNormal]];
}

- (NSString *)cellTitle {
    return [_titleLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"*"]];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    switch (_type) {
        case kConstituency : {
            if (_owner.record.constituencies == 0) return NO;
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[self cellTitle] message:nil preferredStyle:UIAlertControllerStyleActionSheet];
            for (NSString *constituency in _owner.record.constituencies) {
                [alertController addAction:[UIAlertAction actionWithTitle:constituency style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [_textField setText:action.title];
                    [_owner constituencyOfIndexSelected:[_owner.record.constituencies indexOfObject:constituency]];
                }]];
            }
            [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
            [_owner presentViewController:alertController animated:YES completion:nil];
            return NO;
        }
            break;
        case kWard :{
            if (_owner.record.wards == 0) return NO;
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[self cellTitle] message:nil preferredStyle:UIAlertControllerStyleActionSheet];
            for (AGWard *aWard in _owner.record.wards) {
                [alertController addAction:[UIAlertAction actionWithTitle:aWard.wardName style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [_textField setText:action.title];
                    [_owner wardOfIndexSelected:[_owner.record.wards indexOfObject:aWard]];
                }]];
            }
            [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
            [_owner presentViewController:alertController animated:YES completion:nil];
            return NO;
        }
            break;
           default:
            return YES;
            break;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    switch (_type) {
        case  kVoterId :
            _owner.record.voterId = textField.text;
            break;
        case kName :
            _owner.record.name = textField.text;
            break;
        case kEmail :
            _owner.record.email = textField.text;
            break;
        case kMobile :
            _owner.record.mobileNumber = textField.text;
            break;
        case kVerify : {
            [_owner showLoadingWithMessage:nil];
            [AppAPI verifyOtp:textField.text].then(^(NSDictionary *response){
                _owner.record.otpVerified = YES;
            }).catch(^(NSError *error){
                [AppToast showMessage:error.userInfo[NSLocalizedDescriptionKey]];
            }).finally(^{
                [_owner hideLoading];
            });
        }
            break;
        default:
            break;
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    switch (_type) {
        case kSuggession :
            _owner.record.feedback = textView.text;
            break;
        default:
            break;
    }
}

@end

@implementation AGSuggestionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureHamburgerButton];
    [self.tableView setTableFooterView:_tableFooter];

#if DEBUG
    [self fillDefaultValues];
#endif
    
    if (self.isTamilLanguage) {
        [_saveButton setTitle:@"சேமிக்க" forState:UIControlStateNormal];
        [self setTitle:@"பரிந்துரை"];
    } else {
        [_saveButton setTitle:@"SAVE" forState:UIControlStateNormal];
        [self setTitle:@"SUGGESSION"];
    }
    [AppAPI constituencies].then(^(NSArray <AGConstituency *>*constituencies){
        _constituencies = constituencies;
        _record.constituencies = [_constituencies valueForKey:@"constituencyName"];
    });
}

- (void)fillDefaultValues {
    [self setRecord:[AGRecord fromJSON:@{@"name":@"test",@"age":@"22",@"voter_id":@"voterid",@"address":@"test ",@"mobile_number":@"9578353705",@"feedback":@"Jkasjkajk"}]];
}

- (AGRecord *)record {
    if (!_record) {
        _record = [AGRecord new];
        _record.constituencies = [_constituencies valueForKey:@"constituencyName"];
    }
    return _record;
}

- (IBAction)saveButtonAction:(id)sender {
    _record.otpVerified = YES;
    if (_record.otpVerified) {
        [self.view endEditing:YES];
        [self showLoadingWithMessage:self.isTamilLanguage ? @"சேமிக்கப்படுகிறது" : @"Saving"];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self hideLoading];
            [self setRecord:nil];
            [self.tableView reloadData];
            if (self.isTamilLanguage) {
                [self showMessage:@"வெற்றிகரமாக சேமிக்கப்பட்டது"];
            } else {
                [self showMessage:@"SAVED SUCCESSFULLY"];
            }
        });
        return;
        /*
         [AppAPI addRecord:_record].then(^{
         if (self.isTamilLanguage) {
         [self showMessage:@"வெற்றிகரமாக சேமிக்கப்பட்டது"];
         } else {
         [self showMessage:@"SAVED SUCCESSFULLY"];
         }
         [self setRecord:nil];
         [self.tableView reloadData];
         }).catch(^(NSError *error){
         [AppToast showMessage:error.userInfo[NSLocalizedDescriptionKey]];
         }).finally(^{
         [self hideLoading];
         });
         */
    } else {
        if (self.isTamilLanguage) {
            [self showMessage:@"நீங்கள் சேமிக்கும் முன் OTP சரிபார்க்க வேண்டும்"];
        } else {
            [self showMessage:@"You need to verify OTP before saving"];
        }
    }
}

// Cell action
- (void)constituencyOfIndexSelected:(NSInteger)index {
    AGConstituency *constituency = _constituencies[index];
    _record.constituencyName = constituency.constituencyName;
    [self showLoadingWithMessage:nil];
    [AppAPI wardsOfConstituency:constituency.cId].then(^(NSArray *wards){
        _record.wards = wards;
    }).catch(^(NSError *error){
        [AppToast showMessage:error.userInfo[NSLocalizedDescriptionKey]];
    }).finally(^{
        [self hideLoading];
    });
}

- (void)wardOfIndexSelected:(NSInteger)index {
    AGWard *ward = _record.wards[index];
    _record.ward = ward.wardName;
    [self showLoadingWithMessage:nil];
    [AppAPI sectionsOfWard:ward.wId].then(^(NSArray *sections){
        _record.sections = sections;
    }).catch(^(NSError *error){
        [AppToast showMessage:error.userInfo[NSLocalizedDescriptionKey]];
    }).finally(^{
        [self hideLoading];
    });
}

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return kSuggession + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AGSuggestionCell *cell = [tableView dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"%@%d",NSStringFromClass([AGSuggestionCell class]),(int)indexPath.row] forIndexPath:indexPath];
    [cell setOwner:self];
    [cell configureCellWith:indexPath.row];
    return cell;
}

#pragma mark UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case kSuggession :
            return 100;
            break;
        default:
            return 60;
            break;
    }
}

@end
