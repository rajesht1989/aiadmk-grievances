//
//  ViewController.m
//  BusinessCard
//
//  Created by Rajesh on 5/7/16.
//  Copyright © 2016 Org. All rights reserved.
//

#import "BCViewController.h"
#import "MBProgressHUD.h"
#import "UIColor+AppColors.h"
#import "AGHamburgerViewController.h"
#import "AGInitialViewController.h"
#import "GrievancesViewController.h"
#import "AGSuggestionViewController.h"
#import "AGWelfareViewController.h"

@implementation BCNavigationController

+ (void)hamburgerActionReceived:(NSInteger)type {
    UINavigationController *navigationController = (UINavigationController *)[[[UIApplication sharedApplication] keyWindow] rootViewController];
    switch (type) {
        case kHome :
            [navigationController setViewControllers:@[[AGInitialViewController.class instance]]];
            break;
        case kGrievance :
            [navigationController setViewControllers:@[[GrievancesViewController.class instance]]];
            break;
        case kWelfareScheme :
            [navigationController setViewControllers:@[[AGWelfareViewController.class instance]]];
            break;
        case kSuggestion :
            [navigationController setViewControllers:@[[AGSuggestionViewController.class instance]]];
            break;
        default :
            break;
/*
            [navigationController setViewControllers:@[[ADMainViewController.class instance]]];
            break;
        case kProfile :
            [navigationController setViewControllers:@[[ADSignupViewController.class instance]]];
            break;
        case kOrderHistory :
            [navigationController setViewControllers:@[[ADOrderHisViewController.class instance]]];
            break;
        case kFeedBack :
            [navigationController setViewControllers:@[[ADFeedbackViewController.class instance]]];
            break;
        case kOffers :
            [navigationController setViewControllers:@[[ADOffersViewController.class instance]]];
            break;
        case kAboutUs :
            [navigationController setViewControllers:@[[ADAboutUsViewController.class instance]]];
            break;
        case kLogout :
            [ADUser clear];
            [navigationController setViewControllers:@[[ADLoginViewController.class instance]]];
            break;
        default:
            break;
 */
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationBar.barTintColor = [UIColor appGreenColor];
    self.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationBar.translucent = NO;
    self.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return self.topViewController.preferredStatusBarStyle;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return [self.topViewController supportedInterfaceOrientations];
}

@end

@implementation UIViewController (BCViewController)

+ (UIStoryboard *)mainStoryboard {
    return [UIStoryboard storyboardWithName:[[NSBundle mainBundle].infoDictionary objectForKey:@"UIMainStoryboardFile"] bundle:[NSBundle mainBundle]];
}

+ (instancetype)instance {
    return [[self mainStoryboard] instantiateViewControllerWithIdentifier:NSStringFromClass(self)];
}

- (BOOL)shouldHideNavBar {
    return NO;
}

- (BOOL)shouldShowRightCallButton {
    return YES;
}

- (void)showMessage:(NSString *)message {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:alertController animated:YES completion:nil];
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleCancel handler:nil]];
}


- (void)configureTapGestureToResignKeyboard {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [tap setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:tap];
}

- (void)dismissKeyboard {
    [self.view endEditing:YES];
}

#pragma mark Showloading

- (void)showLoadingWithMessage:(NSString *)message {
    [self hideLoading];
    UIView *superView = self.navigationController ? self.navigationController.view : self.view;
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:superView animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    message = message.length ? message : @"Loading";
    hud.labelText = message;
}

- (void)hideLoading {
    UIView *superView = self.navigationController ? self.navigationController.view : self.view;
    [MBProgressHUD hideHUDForView:superView animated:YES];
}

#pragma mark BarConfig

- (void)configureHamburgerButton {
    UIButton *hamburgerButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    [hamburgerButton setImage:[UIImage imageNamed:@"ic_hamburger"] forState:UIControlStateNormal];
    [hamburgerButton setImageEdgeInsets:UIEdgeInsetsMake(0, -15, 0, 0)];
    [hamburgerButton.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [hamburgerButton addTarget:self action:@selector(hamburgerAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:hamburgerButton]];
}

- (void)hamburgerAction:(id)sender {
    [AGHamburgerViewController showHamburger];
}

- (void)configureBackBarButton {
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    [backButton addTarget:self action:@selector(backBarButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setImage:[UIImage imageNamed:@"ic_back"] forState:UIControlStateNormal];
    [backButton setImageEdgeInsets:UIEdgeInsetsMake(0, -30, 0, 0)];
    [backButton.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:backButton]];
}

- (void)configureLogoutButton {
    UIButton *logoutButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [logoutButton addTarget:self action:@selector(logoutButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [logoutButton setImage:[UIImage imageNamed:@"ic_logout"] forState:UIControlStateNormal];
    [logoutButton setImageEdgeInsets:UIEdgeInsetsMake(10, 5, 10, -15)];
    [logoutButton.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:logoutButton]];
}

- (void)configureCancelBarButton {
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(backBarButtonAction:)]];
}

- (void)configureTitleView {
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0,0,200,44)];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_kaching_logo"]];
    [imageView setFrame:CGRectInset(bgView.bounds, 5, 2)];
    [imageView setContentMode:UIViewContentModeScaleAspectFit];
    [imageView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [bgView addSubview:imageView];
    self.navigationItem.titleView = bgView;
}

- (void)backBarButtonAction:(UIButton *)backButton {
    if ([[self.navigationController viewControllers] indexOfObject:self]) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)logoutButtonAction:(UIButton *)backButton {
   /* UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"Are you sure you want to logout?" preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:alertController animated:YES completion:nil];
    [alertController addAction:[UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [self showLoadingWithMessage:@"Logging Out..."];
        [KCAPI logout].catch(^(NSError *error){
            [KCToast showMessage:error.userInfo[NSLocalizedDescriptionKey]];
        }).finally(^{
            [self hideLoading];
            [KCUser clear];
            [self.navigationController setViewControllers:@[[self.storyboard instantiateViewControllerWithIdentifier:@"BCLoginViewController"]] animated:NO];
        });
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:nil]];
    */
}

- (void)hamburgerBarButtonAction:(UIButton *)backButton {
}

#pragma mark KeyboardLogic

- (BOOL)shouldRespondForKeyboardNotification {
    return NO;
}

- (void)keyboardWillShow:(NSNotification *)notification {
    [self adjustTextViewByKeyboardState:YES keyboardInfo:[notification userInfo]];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    [self adjustTextViewByKeyboardState:NO keyboardInfo:[notification userInfo]];
}

- (void)adjustTextViewByKeyboardState:(BOOL)showKeyboard keyboardInfo:(NSDictionary *)info {
    UIViewAnimationCurve animationCurve = [info[UIKeyboardAnimationCurveUserInfoKey] unsignedIntegerValue];
    UIViewAnimationOptions animationOptions = UIViewAnimationOptionBeginFromCurrentState;
    if (animationCurve == UIViewAnimationCurveEaseIn) {
        animationOptions |= UIViewAnimationOptionCurveEaseIn;
    }
    else if (animationCurve == UIViewAnimationCurveEaseInOut) {
        animationOptions |= UIViewAnimationOptionCurveEaseInOut;
    }
    else if (animationCurve == UIViewAnimationCurveEaseOut) {
        animationOptions |= UIViewAnimationOptionCurveEaseOut;
    }
    else if (animationCurve == UIViewAnimationCurveLinear) {
        animationOptions |= UIViewAnimationOptionCurveLinear;
    }
    [UIView animateWithDuration:[[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue] delay:0 options:animationOptions animations:^{
        [self.view layoutIfNeeded];
    }                completion:nil];
}

- (void)setIsTamilLanguage:(BOOL)isTamil {
    [[NSUserDefaults standardUserDefaults] setBool:isTamil forKey:@"isTamilLanguage"];
}
- (BOOL)isTamilLanguage {
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"isTamilLanguage"];
}


@end

@interface BCViewController ()

@end

@implementation BCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if ([self navigationController]) {
        [[self navigationController] setNavigationBarHidden:[self shouldHideNavBar] animated:YES];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if ([self shouldRespondForKeyboardNotification]) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillShow:)
                                                     name:UIKeyboardWillShowNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillHide:)
                                                     name:UIKeyboardWillHideNotification
                                                   object:nil];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if ([self navigationController]) {
        [[self navigationController] setNavigationBarHidden:[self shouldHideNavBar] animated:YES];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    if ([self shouldRespondForKeyboardNotification]) {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
    [super viewDidDisappear:animated];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
        return UIInterfaceOrientationMaskPortrait;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

@implementation  BCTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if ([self navigationController]) {
        [[self navigationController] setNavigationBarHidden:[self shouldHideNavBar] animated:YES];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if ([self shouldRespondForKeyboardNotification]) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillShow:)
                                                     name:UIKeyboardWillShowNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillHide:)
                                                     name:UIKeyboardWillHideNotification
                                                   object:nil];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if ([self navigationController]) {
        [[self navigationController] setNavigationBarHidden:[self shouldHideNavBar] animated:YES];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    if ([self shouldRespondForKeyboardNotification]) {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
    [super viewDidDisappear:animated];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}

@end