//
//  NSObject+FontUtil.h
//
//  Created by Rajesh on 1/16/16.
//

#import "Colours.h"

@interface UIColor (AppColors)

+ (UIImage *)imageWithColor:(UIColor *)color;
+ (UIColor *)appRedColor;
+ (UIColor *)darkRedColor;
+ (UIColor *)darkBlueColor;
+ (UIColor *)appGreenColor;
+ (UIColor *)silverColor;
+ (UIColor *)blackTextColor;
+ (UIColor *)secondaryTextColor;
+ (UIColor *)toastWarningColor;
+ (UIColor *)toastSuccessColor;
+ (UIColor *)toastErrorColor;
+ (UIColor *)toastInfoColor;

@end
