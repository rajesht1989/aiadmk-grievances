//
// Created by Rajesh  on 27/11/15.
// Copyright (c) 2015 Rajesh. All rights reserved.
//


#import "SDiPhoneVersion.h"

typedef enum {
    AESizeTypeBlock,
    AESizeTypeSpacing,
    AESizeTypeBar
} AESizeType;

@interface AppElementSize : NSObject

+ (CGFloat)height:(CGFloat)height;

+ (CGFloat)sizeForDevice:(CGFloat)size;

+ (CGFloat)blockSize:(CGFloat)count;

+ (CGFloat)spacingSize:(CGFloat)count;

+ (CGFloat)sizeWithType:(AESizeType)type;

@end