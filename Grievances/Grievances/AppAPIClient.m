//
// Created by Rajesh  on 6/9/15.
// Copyright (c) 2015 Rajesh. All rights reserved.
//

#import "AFNetworkActivityConsoleLogger.h"
#import "AFNetworkActivityLogger.h"
#import "NSString+Additions.h"
#import "AppAPIClient.h"

@implementation AppAPIClient {

}

static NSDictionary *settings;

+ (AppAPIClient *)sharedClient {
    static AppAPIClient *sharedClient = nil;
    @synchronized (self) {
        if (sharedClient == nil) {
            settings = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"plist"]];
            sharedClient = [[self alloc] init];
            sharedClient.requestSerializer = [AFJSONRequestSerializer serializer];
            sharedClient.responseSerializer = [AFHTTPResponseSerializer serializer];
            [sharedClient setHeaders];
            
#ifdef DEBUG
            AFNetworkActivityConsoleLogger *consoleLogger = [AFNetworkActivityConsoleLogger new];
            [consoleLogger setLevel:AFLoggerLevelDebug];
            [[AFNetworkActivityLogger sharedLogger] removeLogger:[[[AFNetworkActivityLogger sharedLogger] loggers] anyObject]];
            [[AFNetworkActivityLogger sharedLogger] addLogger:consoleLogger];
            [[AFNetworkActivityLogger sharedLogger] startLogging];
#endif
        }
    }
    return sharedClient;
}

+ (NSMutableString *)endpoint:(NSString *)endpoint, ... {
    va_list args;
    va_start(args, endpoint);
    NSString *path = [[NSString alloc] initWithFormat:settings[endpoint] arguments:args];
    va_end(args);
    NSMutableString *url = [NSMutableString stringWithFormat:@"%@%@", [[self sharedClient] apiUrl], path];
    return url;
}

+ (AFPromise *)onResponse:(AFPromise *)promise {
    return promise.then(^(id response, AFHTTPSessionManager *operation) {
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableLeaves error:nil];
        BOOL status = [[responseDict objectForKey:@"status"] boolValue];
        if (!status) {
            @throw [NSError errorWithDomain:NSCocoaErrorDomain code:0 userInfo:@{NSLocalizedDescriptionKey : responseDict[@"message"]}];
        }
        return responseDict;
    }).catch(^(NSError *error) {
        return [NSError errorWithDomain:NSCocoaErrorDomain code:0 userInfo:@{NSLocalizedDescriptionKey : error.localizedDescription}];
    });
}

+ (AFPromise *)GET:(NSString *)urlString parameters:(id)parameters {
    if (parameters) urlString = [NSString stringWithFormat:@"%@?%@",urlString,[NSString paramString:parameters]];
    else urlString = urlString;
    return [self onResponse:[[self sharedClient] GET:urlString parameters:parameters]];
}

+ (AFPromise *)HEAD:(NSString *)urlString parameters:(id)parameters {
    return [self onResponse:[[self sharedClient] HEAD:urlString parameters:parameters]];
}

+ (AFPromise *)POST:(NSString *)urlString parametersAsFormData:(id)parameters {
#ifdef DEBUG
    NSLog(@"%@",parameters);
#endif
    return [self onResponse:[[self sharedClient] POST:urlString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        for (NSString *aKey in parameters) {
            if ([[parameters objectForKey:aKey] isKindOfClass:[NSString class]]) {
                [formData appendPartWithFormData:[[parameters objectForKey:aKey] dataUsingEncoding:NSUTF8StringEncoding] name:aKey];
            } else {
                [formData appendPartWithFormData:[parameters objectForKey:aKey] name:aKey];
            }
        }
    }]];
}

+ (AFPromise *)POST:(NSString *)urlString parameters:(id)parameters {
    return [self onResponse:[[self sharedClient] POST:urlString parameters:parameters]];
}

- (instancetype)init {
    if (self = [super initWithBaseURL:[NSURL URLWithString:[self apiUrl]]]) {
    }
    return self;
}

- (void)setHeaders {
    [self.requestSerializer setValue:@"application/json; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [self.requestSerializer setValue:@"Cache-Control" forHTTPHeaderField:@"no-cache"];
    self.responseSerializer.acceptableContentTypes = [self.responseSerializer.acceptableContentTypes setByAddingObject:@"application/vnd.api+json"];
}

- (NSString *)apiUrl {
    return settings[@"api_base_url"];
}

@end
