//
//  ViewController.h
//  BusinessCard
//
//  Created by Rajesh on 5/7/16.
//  Copyright © 2016 Org. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface BCNavigationController : UINavigationController

+ (void)hamburgerActionReceived:(NSInteger)type;

@end

@interface UIViewController (BCViewController)

#pragma mark Instance

+ (UIStoryboard *)mainStoryboard;

+ (instancetype)instance;

- (void)configureTapGestureToResignKeyboard;

#pragma mark Showloading

- (void)showLoadingWithMessage:(NSString *)message;

- (void)hideLoading;

#pragma mark ShouldHide

- (BOOL)shouldHideNavBar;

- (BOOL)shouldShowRightCallButton;

- (void)showMessage:(NSString *)message;

#pragma mark Navbar

- (void)configureBackBarButton;

- (void)configureCancelBarButton;

- (void)configureHamburgerButton;

- (void)configureLogoutButton;

- (void)configureTitleView;

#pragma mark KeyboardLogic

- (BOOL)shouldRespondForKeyboardNotification;

- (void)adjustTextViewByKeyboardState:(BOOL)showKeyboard keyboardInfo:(NSDictionary *)info;

- (void)setIsTamilLanguage:(BOOL)isTamil;

- (BOOL)isTamilLanguage;

@end

@interface BCViewController: UIViewController

@end

@interface BCTableViewController: UITableViewController

@end
