//
// Created by Rajesh  on 27/11/15.
// Copyright (c) 2015 Rajesh. All rights reserved.
//

#import "AppElementSize.h"

@implementation AppElementSize {

}

+ (CGFloat)height:(CGFloat)height {
    CGFloat heightToReturn;
    switch ([SDiPhoneVersion deviceSize]) {
        case iPhone55inch:
        heightToReturn = (CGFloat) (height * 1.53);
        break;
        case iPhone47inch:
        heightToReturn = (CGFloat) (height * 1.39);
        break;
        case iPhone4inch:
        heightToReturn = (CGFloat) (height * 1.18);
        break;
        default:
        heightToReturn = height;
        break;
    }
    return (CGFloat) ceil(heightToReturn);
}

+ (CGFloat)sizeForDevice:(CGFloat)size {
    CGFloat sizeToReturn;
    switch ([SDiPhoneVersion deviceSize]) {
        case UnknownSize :
            sizeToReturn = (CGFloat) (size * 1.3);
            break;
        case iPhone55inch :
            sizeToReturn = (CGFloat) (size * 1.17);
            break;
        case iPhone47inch :
            sizeToReturn = (CGFloat) (size * 1);
            break;
        case iPhone4inch :
            sizeToReturn = (CGFloat) (size * .85);
            break;
        case iPhone35inch :
            sizeToReturn = (CGFloat) (size * .74);
            break;
        default :
            sizeToReturn = size;
            break;
    }
    return (CGFloat) ceil(sizeToReturn);
}

+ (CGFloat)blockSize:(CGFloat)count {
    return [self sizeWithType:AESizeTypeBlock] * count;
}

+ (CGFloat)spacingSize:(CGFloat)count {
    return [self sizeWithType:AESizeTypeSpacing] * count;
}

+ (CGFloat)sizeWithType:(AESizeType)type{
    float size = 1;
    switch (type) {
        case AESizeTypeBlock:
            size = 50;
            break;
        case AESizeTypeSpacing:
            size = 10;
            break;
        case AESizeTypeBar:
            return 55;
    }
    return [self sizeForDevice:size];
}
@end